# simple_pi

Description. 
The package simple_pi is used to:
	- 
	-

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install simple_pi

```bash
pip install simple_pi
```

## Usage

```python
from simple_pi import pi
pi.calculate(10)
```

## Author
Diego Soek

## License
[MIT](https://choosealicense.com/licenses/mit/)